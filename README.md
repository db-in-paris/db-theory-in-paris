# DB Theory in Paris

If you encounter a bug or have some thoughts regarding this project, you can send me an email at : rogova [at] irif [dot] fr

### What it is

A way to centralise everything that's happening around database research (at least theory side) in Paris.

Always know if there is an interesting talk, a colleague published a new paper, someone joined/left a team, ...

### The website

Currently, the [website](https://db-in-paris.gitlab.io/db-theory-in-paris/) is a Gitlab Page generated with Gatsby using this repo. 
It contains: 
- A landing page with the 20 last published articles
- An archive of the newsletter
- A list of past and future talks with calendar export for each
- A list of current members with affiliation, short description and link to dblp page
- A search function

Some features are still missing: 
- An overview of the coming talks
- A general calendar updated regularly
- Link to recordings of talks if available [easy]

### The mailing list

Does not exist, yet. There are two main issues : 
1. Maintenance is very annoying. We don't want this to become a side job for whoever is in charge of the project.
2. Vendor/lab lock-in. If one of the participating labs accepts to host the mailing list, it will be hard to administer for someone not from that lab (at least IRIF is annoying with this). 

### Members

**Difference between team and individual membership**
If a *team* is a member of this project, all database theory related talks happening at that team's seminar/working group/meeting will be published on the website.
All *individual* members, will have a personal page on the website and news related to them might be communicated via the newsletter.
There is no correlation between team and individual memberships and both are cancellable at any time.

##### Teams
- Équipe automates, IRIF
- Équipe BAAM, LIGM
- Équipe VALDA, Inria

##### Individuals

- Sylvain Schmitz, PR, IRIF
- Alexandra Rogova, PhD student, IRIF