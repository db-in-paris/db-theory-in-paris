/**
 * @type {import('gatsby').GatsbyConfig}
 */
module.exports = {
  pathPrefix: `db-theory-in-paris/`,
  siteMetadata: {
    title: `Database Theory in Paris v2`,
    siteUrl: `https://db-in-paris.gitlab.io/db-theory-in-paris`
  },
  plugins: ["gatsby-transformer-remark",
    "gatsby-plugin-mdx",
    "gatsby-transformer-json",
    "gatsby-plugin-sass",
    "react-use-flexsearch",
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        "name": `newsletters`,
        "path": `${__dirname}/data/newsletters`
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        "name": `other`,
        "path": `${__dirname}/data/other`
      },
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        name: 'members',
        engine: 'flexsearch',
        engineOptions: {
          tokenize: 'full'
        },
        query: `
        {
          allMembersJson {
            nodes {
              description
              name
              main_affiliation
              id
            }
          }
        }
      `,
        ref: 'id',
        normalizer: ({ data }) =>
          data.allMembersJson.nodes.map((node) => ({
            id: node.id,
            description: node.description,
            name: node.name,
            main_affiliation: node.main_affiliation,
          })),
      }
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        name: 'talks',
        engine: 'flexsearch',
        engineOptions: {
          tokenize: 'full'
        },
        query: `
        {
          allTalksJson {
            nodes {
              id
              location
              date
              comparisonDate: date(formatString: "YYYY-MM-DD")
              time
              title
              speaker
              abstract
            }
          }
        }
      `,
        ref: 'id',
        normalizer: ({ data }) =>
          data.allTalksJson.nodes.map((node) => ({
            id: node.id,
            location: node.location,
            date: node.date,
            comparisonDate: node.comparisonDate,
            time: node.time,
            title: node.title,
            speaker: node.speaker,
            abstract: node.abstract
          })),
      }
    },
    {
      resolve: 'gatsby-plugin-local-search',
      options: {
        name: 'newsletter',
        engine: 'flexsearch',
        engineOptions: {
          tokenize: 'full'
        },
        query: `
        {
          allMdx {
            nodes {
              body
              id
              frontmatter {
                date(formatString: "MMMM Do, YYYY")
                title
                slug
              }
            }
          }
        }
      `,
        ref: 'id',
        normalizer: ({ data }) =>
          data.allMdx.nodes.map((node) => ({
            id: node.id,
            body: node.body,
            date: node.frontmatter.date,
            title: node.frontmatter.title,
            slug: node.frontmatter.slug
          })),
      }
    }
  ]
};