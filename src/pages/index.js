import * as React from 'react'
import GeneralLayout from '../components/general-layout'
import { graphql } from 'gatsby'
import moment from 'moment'

const IndexPage = ({ data }) => {
  const formattedMdx = data.allMdx.nodes.map(item => {
    return {
      id: item.id,
      title: item.frontmatter.title,
      body: item.body,
      footer: item.frontmatter.date,
      comparisonDate: item.frontmatter.comparisonDate,
      link: "/newsletter/" + item.frontmatter.slug
    }
  })

  const formattedJson = data.allTalksJson.nodes.map(item => {
    return {
      id: item.id,
      title: item.title + " · " + item.speaker,
      body: item.abstract,
      footer: item.date + " · " + item.location,
      comparisonDate: item.comparisonDate,
      link: "/talks/" + item.comparisonDate
    }
  })

  const queryResult = formattedJson.concat(formattedMdx).sort(function (a, b) {
    if (moment(a.comparisonDate).isBefore(moment(b.comparisonDate))) return 1
    if (moment(b.comparisonDate).isBefore(moment(a.comparisonDate))) return -1
    else return 0
  }).slice(0, 20)

  const paginationInfo = {
    total: 1,
    current: 1,
    path: "/"
  }

  return (
    <GeneralLayout title="Recently published" postList={queryResult} paginationInfo={paginationInfo} />
  )
}

export const query = graphql`
query {
  allTalksJson(limit: 20){
    nodes {
      title
    }
  }
  allTalksJson(limit: 20) {
    nodes {
      id
      title
      speaker
      location
      date(formatString: "MMMM Do, YYYY")
      comparisonDate: date(formatString: "YYYY-MM-DD")
      abstract
    }
  }
  allMdx(sort: {frontmatter: {title: ASC}}, limit: 20) {
      nodes {
        body
        id
        frontmatter {
          date(formatString: "MMMM Do, YYYY")
          comparisonDate: date(formatString: "YYYY-MM-DD")
          title
          slug
        }
      }
    }
  site {
    siteMetadata {
      title
    }  
  }
}
`

export const Head = ({ data }) => {
  return (
    <>
      <html lang="en" />
      <title>{data.site.siteMetadata.title}</title>
      <body className="has-navbar-fixed-top" />
    </>
  )
}

export default IndexPage