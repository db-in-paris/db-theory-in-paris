import * as React from 'react'
import { graphql } from 'gatsby'
import SinglePostLayout from '../../components/single-post-layout'

const MemberPost = ({ data }) => {
    const postContent = (
        <div className='container'>
            <main>
                <div className='meta-info'>
                    <p>Main affiliation: {data.membersJson.main_affiliation} | <a href={data.membersJson.personal_page}>Personal page</a> | <a href={data.membersJson.dblp_link}>DBLP page</a></p>
                </div>
                <div className='member-description'>
                    {data.membersJson.description}
                </div>
            </main>
        </div>
    )

    return (
        <SinglePostLayout title={data.membersJson.name} postContent={postContent} />
    )
}

export const query = graphql`
  query ($id: String) {
    membersJson(id: {eq: $id}) {
        name
        main_affiliation
        description
        dblp_link
        personal_page
    }
  }
`

export const Head = ({ data }) => <title>{data.membersJson.name}</title>

export default MemberPost