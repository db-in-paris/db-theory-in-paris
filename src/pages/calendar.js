import * as React from 'react'
import SinglePostLayout from '../components/single-post-layout'

const CalendarPage = () => {
  const postContent = (
    <div className='container'>
      <main>
        <div>
          Under construction. <br />
          What will be here : <br />
          - A list of upcoming events (possible calendar view?) <br />
          - Subscribe to general ICS form <br />
        </div>
      </main>
    </div>
  )

  return (
    <SinglePostLayout title="About" postContent={postContent} />
  )
}

export const Head = () => <title>Calendar</title>

export default CalendarPage