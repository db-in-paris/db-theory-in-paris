import * as React from 'react'
import { graphql } from 'gatsby'
import SinglePostLayout from '../../components/single-post-layout'

const NewsletterPost = ({ data, children }) => {
  const postContent = (
    <div className='container'>
      <main>
        {children}
      </main>
    </div>
  )

  return (
    <SinglePostLayout title={data.mdx.frontmatter.title} postContent={postContent} />
  )
}

export const query = graphql`
  query ($id: String) {
    mdx(id: {eq: $id}) {
      frontmatter {
        title
        date(formatString: "MMMM D, YYYY")
      }
    }
  }
`

export const Head = ({ data }) => <title>{data.mdx.frontmatter.title}</title>

export default NewsletterPost