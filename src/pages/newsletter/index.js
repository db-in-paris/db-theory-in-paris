import * as React from 'react'
import GeneralLayout from '../../components/general-layout'
import { useStaticQuery, graphql } from 'gatsby'

const IndexPage = () => {
  const query = useStaticQuery(graphql`
  query {
    allMdx(sort: {frontmatter: {title: ASC}}) {
        nodes {
          body
          id
          frontmatter {
            date(formatString: "MMMM Do, YYYY")
            title
            slug
          }
        }
      }
  }
`)

  const formattedMdx = query.allMdx.nodes.map(item => {
    return {
      id: item.id,
      title: item.frontmatter.title,
      body: item.body,
      footer: item.frontmatter.date,
      link: "/newsletter/" + item.frontmatter.slug
    }
  })

  return (
    <GeneralLayout title="Newsletter archive" postList={formattedMdx} />
  )
}

export const Head = () => <title>Newsletter archive</title>
export default IndexPage