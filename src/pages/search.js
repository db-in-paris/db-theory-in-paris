import * as React from 'react'
import ToolBar from '../components/toolbar'
import Footer from '../components/footer'
import { useState } from 'react'
import { graphql } from 'gatsby'
import Card from '../components/card'
import { useFlexSearch } from 'react-use-flexsearch'

function formatResult(res, type) {
  const formatted = {}
  if (type === "member") {
    formatted.id = res.id
    formatted.title = res.name
    formatted.body = res.description
    formatted.footer = res.affiliation
    formatted.link = "/members/" + (res.name.replace(" ", "-").toLowerCase())
  } else if (type === "talk") {
    formatted.id = res.id
    formatted.title = res.title + " · " + res.speaker
    formatted.body = res.abstract
    formatted.footer = res.date + " · " + res.location
    formatted.link = "/talks/" + res.comparisonDate
  } else if (type === "newsletter") {
    formatted.id = res.id
    formatted.title = res.frontmatter.title
    formatted.body = res.body
    formatted.footer = res.frontmatter.date
    formatted.link = "/newsletter/" + res.frontmatter.slug
  }
  return formatted
}

const SearchResult = ({ searchValue, index, store, type }) => {
  const results = useFlexSearch(searchValue, index, store, { tokenize: 'full' })
  return (
    results.map(res =>
    (
      <li key={res.id}>
        <Card content={formatResult(res, type)} />
      </li>
    ))
  )
}

const IndexPage = ({ data }) => {
  const membersIndex = data.localSearchMembers.index
  const membersStore = data.localSearchMembers.store
  const talksIndex = data.localSearchTalks.index
  const talksStore = data.localSearchTalks.store
  const newsletterIndex = data.localSearchNewsletter.index
  const newsletterStore = data.localSearchNewsletter.store

  const [searchValue, setSearchValue] = useState('')
  const [searchResult, setSearchResult] = useState('')

  function handleSubmit(e) {
    e.preventDefault()
    const newResults = (
      <div>
        <SearchResult searchValue={searchValue} index={membersIndex} store={membersStore} type="member" />
        <SearchResult searchValue={searchValue} index={talksIndex} store={talksStore} type="talk" />
        <SearchResult searchValue={searchValue} index={newsletterIndex} store={newsletterStore} type="newsletter" />
      </div>
    )
    setSearchResult(newResults)
  }

  return (
    <div>
      <ToolBar></ToolBar>
      <div className='columns is-mobile is-centered main-container'>
        <div className="column is-half" id="main-column">
          <form method='post' onSubmit={handleSubmit}>
            <div className='field has-addons' id='search-form'>
              <div className='control is-expanded is-large'>
                <input name="search-input" className="input" type="text" placeholder="Search for talks, members or newsletters" value={searchValue} onChange={e => setSearchValue(e.target.value)} />
              </div>
              <div className='control'>
                <button className='button'>Search</button>
              </div>
            </div>
          </form>
          <ul className='search-results'>
            {searchResult}
          </ul>
        </div>
      </div>
      <footer className="footer" id='page-footer'>
        <div className="content has-text-centered">
          <Footer></Footer>
        </div>
      </footer>
    </div>
  )
}

export const query = graphql`
query {
  localSearchMembers {
    index
    store
  }
  localSearchTalks {
    index
    store
  }
  localSearchNewsletter {
    index
    store
  }
}
`

export const Head = () => <title>Search</title>
export default IndexPage