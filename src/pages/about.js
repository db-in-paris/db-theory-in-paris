import * as React from 'react'
import SinglePostLayout from '../components/single-post-layout'

const AboutPage = () => {
  const postContent = (
    <div className='container'>
      <main>
        <div>
          A way to centralise everything that's happening around database research (at least theory side) in Paris (IRIF, Marne & Ulm for now).
          Always know if there is an interesting talk, a colleague published a new paper, someone joined/left a team, ...
        </div>
      </main>
    </div>
  )

  return (
    <SinglePostLayout title="About" postContent={postContent} />
  )
}

export const Head = () => <title>About</title>

export default AboutPage