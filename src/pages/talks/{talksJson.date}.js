import * as React from 'react'
import { graphql } from 'gatsby'
import SinglePostLayout from '../../components/single-post-layout'
import * as ics from 'ics'

const TalkPost = ({ data }) => {

    function generateCalendar() {
        let formattedDate = data.talksJson.icsDate.split("-")
        formattedDate = formattedDate.concat(data.talksJson.time.split(":"))
        formattedDate = formattedDate.map(el => parseInt(el))

        const cal = ics.createEvent({
            start: formattedDate,
            duration: { hours: 1, minutes: 0 },
            title: data.talksJson.title,
            description: data.talksJson.abstract,
            location: data.talksJson.location
        })

        const calBlob = new Blob([cal.value], { type: "text/calendar" })
        const calUrl = window.URL.createObjectURL(calBlob)
        const fakeLink = document.createElement('a')
        fakeLink.style = "display: none"
        fakeLink.href = calUrl
        fakeLink.download = data.talksJson.title + ".ics"
        document.body.appendChild(fakeLink)
        fakeLink.click()
        window.URL.revokeObjectURL(calUrl)
    }

    const postContent = (
        <div className='container'>
            <main>
                <div className='meta-info'>
                    <div> {data.talksJson.speaker} | {data.talksJson.date} at {data.talksJson.time} | {data.talksJson.location} <button className='button add-ics-button' onClick={generateCalendar}>Add to calendar</button> </div>
                </div>
                <div className='member-description'>
                    {data.talksJson.abstract}
                </div>
            </main>
        </div>
    )

    return (
        <SinglePostLayout title={data.talksJson.title} postContent={postContent} />
    )
}

export const query = graphql`
  query ($id: String) {
    talksJson(id: {eq: $id}) {
        date(formatString: "MMMM Do, YYYY")
        icsDate: date(formatString: "YYYY-M-D")
        time
        speaker
        location
        title
        abstract
    }
  }
`

export const Head = ({ data }) => <title>{data.talksJson.title}</title>

export default TalkPost