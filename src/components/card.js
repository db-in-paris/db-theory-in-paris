import * as React from 'react'
import { Link } from 'gatsby'

export default function Card({ content }) {
    return (
        <div className='card'>
            <Link to={content.link}>
                <header className='card-header'>
                    <div className='card-header-title'>
                        {content.title}
                    </div>
                </header>
                <div className='card-content'>
                    <div className='content'>
                        {content.body}
                    </div>
                </div>
                <footer className='card-footer'>
                    <div className='card-footer-item'>
                        {content.footer}
                    </div>
                </footer>
            </Link>
        </div>
    )
}

