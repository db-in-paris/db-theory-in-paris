import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

export default function Footer() {
  const data = useStaticQuery(graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }`)

  return (
    <p>{data.site.siteMetadata.title} by Alexandra Rogova. Built using <a href="https://www.gatsbyjs.com/">Gatsby</a> and <a href="https://bulma.io/">Bulma</a></p>
  )
}