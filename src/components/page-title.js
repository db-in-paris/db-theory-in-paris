import * as React from 'react'

export default function PageTitle({ title }) {
    return (
        <div className='container has-text-centered'>
            <h2 className='title page-title'>{title}</h2>
        </div>
    )
}