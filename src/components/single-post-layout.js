import React from "react"
import "../style/bulmastyles.scss"
import "../style/style.css"
import "../style/md-style.css"
import ToolBar from "./toolbar"
import Footer from "./footer"
import PageTitle from "./page-title"

const SinglePostLayout = ({ title, postContent }) => {
  // const isMobile = navigator.userAgent.toLowerCase().includes("mobile")

  return (
    <div>
      <ToolBar></ToolBar>
      <div className="columns is-mobile is-centered main-container">
        {/* <div className={`column ${isMobile ? "is-full" : "is-half"}`} id="main-column"> */}
        <div className="column is-three-quarters" id="main-column">
          <PageTitle title={title} />
          {postContent}
        </div>
      </div>
      <footer className="footer" id='page-footer'>
        <div className="content has-text-centered">
          <Footer></Footer>
        </div>
      </footer>
    </div >
  )
}
export default SinglePostLayout