import React from "react"
import "../style/bulmastyles.scss"
import "../style/style.css"
import ToolBar from "./toolbar"
import Footer from "./footer"
import PageTitle from "./page-title"
import Card from "./card"
import Pagination from "./pagination"

const GeneralLayout = ({ title, postList, paginationInfo }) => {
  if (paginationInfo === undefined) {
    paginationInfo = {
      total: 1,
      current: 1,
      path: ""
    }
  }

  // const isMobile = navigator.userAgent.toLowerCase().includes("mobile")

  return (
    <div>
      <ToolBar></ToolBar>
      <div className="columns is-mobile is-centered main-container">
        {/* <div className={`column ${isMobile ? "is-full" : "is-half"}`} id="main-column"> */}
        <div className="column is-three-quarters" id="main-column">
          <PageTitle title={title} />
          <ul>
            {
              postList.map(item => (
                <li key={item.id}>
                  <Card content={item} />
                </li>
              ))
            }
          </ul>
          <Pagination total={paginationInfo.total} current={paginationInfo.current} path={paginationInfo.path}></Pagination>
        </div>
      </div>
      <footer className="footer" id='page-footer'>
        <div className="content has-text-centered">
          <Footer></Footer>
        </div>
      </footer>
    </div >
  )
}
export default GeneralLayout