import * as React from 'react'
import { useState } from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'

export default function ToolBar() {
  function toggleMobileMenu() {
    SetMobileMenuState(!MobileMenuState)
  }

  const data = useStaticQuery(graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }`)

  const [MobileMenuState, SetMobileMenuState] = useState(false)

  return (
    <nav className='navbar is-fixed-top' role='navigation' aria-label='main navigation'>
      <div className='navbar-brand'>
        <Link className='navbar-item' id="navbar-home-link" to="/">
          {data.site.siteMetadata.title}
        </Link>
        <button className={`navbar-burger ${MobileMenuState ? "is-active" : ""}`} onClick={toggleMobileMenu}>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div className={`navbar-menu ${MobileMenuState ? "is-active" : ""}`}>
        <div className='navbar-start'>

        </div>
        <div className='navbar-end'>
          <Link className='navbar-item' to="/about">
            About
          </Link>
          <Link className='navbar-item' to="/newsletter">
            Newsletter
          </Link>
          <Link className='navbar-item' to="/talks">
            Talks
          </Link>
          <Link className='navbar-item' to="/members">
            Members
          </Link>
          <Link className='navbar-item' to="/calendar">
            Upcoming
          </Link>
          <Link className='navbar-item' to="/search">
            Search
          </Link>
        </div>
      </div>
    </nav>
  )
}