import * as React from 'react'
import { Link } from 'gatsby'

function explicitPagination(total, current, path) {
    const buttons = []
    for (let i = 1; i <= total; i++) {
        buttons.push({
            label: i,
            key: "pag_" + i,
            path: i === 1 ? path : path + "/" + i,
            isCurrent: i === current
        })
    }
    return (
        <ul className='pagination-list'>
            {
                buttons.map(item => (
                    <li key={item.key}>
                        <Link className={`pagination-link ${item.isCurrent ? "is-current" : ""}`} to={item.path}>{item.label}</Link>
                    </li>
                ))
            }
        </ul>
    )
}

function expandFirst(total, current, path) {
    return (
        <ul className='pagination-list'>
            <li key="pag_1"><Link className={`pagination-link ${current === 1 ? "is-current" : ""}`} to={path}>1</Link></li>
            <li key="pag_2"><Link className={`pagination-link ${current === 2 ? "is-current" : ""}`} to={path + "/" + 2}>2</Link></li>
            <li key="pag_3"><Link className={`pagination-link ${current === 3 ? "is-current" : ""}`} to={path + "/" + 3}>3</Link></li>
            {current === 3 ? (<li><Link className={`pagination-link`} to={path + "/" + 4} key="pag_4">4</Link></li>) : null}

            <li key="elipsis-span-first"><span className="pagination-ellipsis">&hellip;</span></li>
            <li key={"pag_" + total}><Link className="pagination-link" to={path + "/" + total}>{total}</Link></li>
        </ul>
    )
}

function expandLast(total, current, path) {
    return (
        <ul className='pagination-list'>
            <li key="pag_1"><Link className="pagination-link" to={path}>1</Link></li>
            <li key="elipsis-span-last"><span className="pagination-ellipsis">&hellip;</span></li>

            {current === (total - 2) ? (<li key={"pag_" + (total - 3)}><Link className={`pagination-link`} to={path + "/" + (total - 3)}>{total - 3}</Link></li>) : null}
            <li key={"pag_" + (total - 2)}><Link className={`pagination-link ${current === total - 2 ? "is-current" : ""}`} to={path + "/" + (total - 2)}>{total - 2}</Link></li>
            <li key={"pag_" + (total - 1)}><Link className={`pagination-link ${current === total - 1 ? "is-current" : ""}`} to={path + "/" + (total - 1)}>{total - 1}</Link></li>
            <li key={"pag_" + total}><Link className={`pagination-link ${current === total ? "is-current" : ""}`} to={path + "/" + total}>{total}</Link></li>
        </ul>
    )

}

function expandMiddle(total, current, path) {
    return (
        <ul className='pagination-list'>
            <li key="pag_1"><Link className="pagination-link" to={path}>1</Link></li>
            <li key="elipsis-span-middle-first"><span className="pagination-ellipsis">&hellip;</span></li>

            <li key={"pag_" + (current - 1)}><Link className="pagination-link" to={path + "/" + (current - 1)}>{current - 1}</Link></li>
            <li key={"pag_" + current}><span className="pagination-link is-current">{current}</span></li>
            <li key={"pag_" + (current + 1)}><Link className="pagination-link" to={path + "/" + (current + 1)}>{current + 1}</Link></li>

            <li key="elipsis-span-middle-last"><span className="pagination-ellipsis">&hellip;</span></li>
            <li key={"pag_" + total}><Link className="pagination-link" to={path + "/" + total}>{total}</Link></li>
        </ul>
    )

}

export default function Pagination({ total, current, path }) {
    if (total === 1) {
        return <div />
    }

    let filler;
    if (total <= 8) {
        filler = explicitPagination(total, current, path)
    } else if (current <= 3) {
        filler = expandFirst(total, current, path)
    } else if (current >= total - 2) {
        filler = expandLast(total, current, path)
    } else {
        filler = expandMiddle(total, current, path)
    }

    const isFirst = current === 1
    const isLast = current === total
    const toPrev = current === 2 ? path : path + "/" + (current - 1)
    const toNext = path + "/" + (current + 1)

    return (
        <nav className='pagination is-centered' role='navigation'>
            <Link className={`pagination-previous ${isFirst ? "is-disabled" : ""}`} to={toPrev}>&lt;</Link>
            <Link className={`pagination-next ${isLast ? "is-disabled" : ""}`} to={toNext}>&gt;</Link>
            {filler}
        </nav>
    )
}