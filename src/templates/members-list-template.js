import * as React from 'react'
import GeneralLayout from '../components/general-layout'
import { graphql } from 'gatsby'

export const membersListQuery = graphql`
query ($skip: Int!, $limit: Int!){
  allMembersJson(
    limit: $limit
    skip: $skip
  ) {
    nodes {
      id
      name
      main_affiliation
      description
    }
  }
}
`

export default class MembersList extends React.Component {
  render() {
    const formattedJson = this.props.data.allMembersJson.nodes.map(item => {
      return {
        id: item.id,
        title: item.name,
        body: item.description,
        footer: item.main_affiliation,
        link: "/members/" + (item.name.replace(" ", "-").toLowerCase())
      }
    })

    const paginationInfo = {
      total: this.props.pageContext.numPages,
      current: this.props.pageContext.currentPage,
      path: "/members"
    }

    return (
      <GeneralLayout title="Members" postList={formattedJson} paginationInfo={paginationInfo} />
    )
  }
}

export const Head = () => <title>Members</title>