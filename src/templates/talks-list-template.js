import * as React from 'react'
import { graphql } from 'gatsby'
import GeneralLayout from '../components/general-layout'

export const talksListQuery = graphql`
query ($skip: Int!, $limit: Int!){
    allTalksJson(
      limit: $limit
      skip: $skip
    ) {
      nodes {
        id
        title
        speaker
        location
        abstract
        date(formatString: "MMMM Do, YYYY")
        comparisonDate: date(formatString: "YYYY-MM-DD")
      }
    }
  }
`

export default class TalksList extends React.Component {
  render() {
    const formattedJson = this.props.data.allTalksJson.nodes.map(item => {
      return {
        id: item.id,
        title: item.title + " · " + item.speaker,
        body: item.abstract,
        footer: item.date + " · " + item.location,
        link: "/talks/" + item.comparisonDate
      }
    })

    const paginationInfo = {
      total: this.props.pageContext.numPages,
      current: this.props.pageContext.currentPage,
      path: "/talks"
    }

    return (
      <GeneralLayout title="Talks" postList={formattedJson} paginationInfo={paginationInfo} />
    )
  }
}

export const Head = () => <title>Talks</title>