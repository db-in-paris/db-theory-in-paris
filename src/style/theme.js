import { createTheme } from '@mui/material/styles';

const Theme = createTheme({
    palette: {
        mode: 'light',
        primary: {
            main: '#f5f5f5',
        },
        secondary: {
            main: '#b39ddb',
        },
        divider: '#ffebee',
        background: {
            paper: '#f5f5f5',
            default: '#f5f5f5'
        }
    }
})

export default Theme;