const path = require("path")
const { createFilePath } = require("gatsby-source-filesystem")
const postsPerPage = 20

function createPages(pageList, pagePath, templatePath, createPageFunction) {
    const numPages = Math.ceil(pageList.length / postsPerPage)
    Array.from({ length: numPages }).forEach((_, i) => {
        createPageFunction({
            path: i === 0 ? `${pagePath}` : `${pagePath}/${i + 1}`,
            component: path.resolve(templatePath),
            context: {
                limit: postsPerPage,
                skip: i * postsPerPage,
                numPages,
                currentPage: i + 1,
            }
        })
    })
}

exports.createPages = async ({ graphql, actions, reporter }) => {
    const { createPage } = actions

    const result = await graphql(`
    {
      allMembersJson {
          nodes {
             name
          }
      }
      allTalksJson {
            nodes {
                date(formatString: "YYYY-MM-DD")
            }
      }
    }
    `
    )

    if (result.errors) {
        reporter.panicOnBuild(`Error while running GraphQL query.`)
        return
    }

    createPages(result.data.allMembersJson.nodes, "/members", "./src/templates/members-list-template.js", createPage)
    createPages(result.data.allTalksJson.nodes, "/talks", "./src/templates/talks-list-template.js", createPage)
}